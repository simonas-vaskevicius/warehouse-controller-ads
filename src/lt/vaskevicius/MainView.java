package lt.vaskevicius;

import lt.vaskevicius.model.Customer;
import lt.vaskevicius.model.Order;
import lt.vaskevicius.model.Product;
import lt.vaskevicius.utils.WarehouseConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*TODO: Programos vartotojas gali:
add number/string validations!
·         +nuskaityti užsakymų sąrašą  (vartotojas įveda pav.),  (0,5 balo)

·        papildyti užsakymų sąrašą nauju užsakymu (konsolėje), (0,5 balo)

·        surasti užsakymą ir jį peržiūrėti pagal:

o   užsakymo numerį, (0,5 balo)

o   kliento vardą ir/arba pavardę, (0,5 balo)

o   adresą ar jo dalį. (0,5 balo)

·        parodyti visus užsakymus (!), (0,4 balo)

·        pakeisti užsakymo būseną į „supakuotas ir išsiųstas“, (0,2 balo)

·        išvalyti sąrašą, (0,2 balo)

·        išeiti iš programos. (0,2 balo)

Nematomos operacijos ir funkcionalumai:

·         pakeitus užsakymo būseną (supakavus ir išsiuntus) programa automatiškai ištrina užsakymą iš eilės ir įrašo į išsiųstų užsakymų dokumentą (praktiškiau būtų pakeisti užsakymo būsena DB), (1 balas)

·         perduoda sandėlio inventoriui, kokių prekių sumažėjo. (0,5 balo)

UI:

·         ekrane visada rodomas programos valdymas ir užsakymo, kurį dabar reikia supakuoti ir išsiųsti, informacija (išskyrus galbūt visų užsakymų rodymo ekrane atveju). (1 balas)

Užsakymų įrašo struktūra: užsakovo vardas ir pavardė, adresas, prekės, užsakymo būsena ir panašiai (studentas gali pasirinkti). (1 balas)*/

public class MainView {

    private MainPresenter mainPresenter;
    private Customer customer;


    public void init() {
        initPresenter();
        try {
            printMenu(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainPresenter.getOrders();
    }

    private void printOrderQueue() {
        List<Order> orders = mainPresenter.getOrders();
        if (orders.size() == 0) {
            System.out.println("No orders found");
        } else {
            System.out.println("\n----- Order to fulfill -----");
            printOrder(orders.get(0));
        }
    }

    private void printMenu(boolean showOrderQueue) throws IOException {
        if (showOrderQueue) {
            printOrderQueue();
        }

        System.out.println("\n\n");
        System.out.println("--- Warehouse controller ---");

        System.out.println("SELECT COMMAND: ");
        System.out.println("DISPLAY LIST - 1");
        System.out.println("NEW ORDER - 2");
        System.out.println("FIND ORDER BY ORDER NUMBER - 3");
        System.out.println("START FRESH COPY OF WAREHOUSE CONTROLLER - 4");
        System.out.println("DISPLAY FINISHED ORDERS - 5");
        System.out.println("EXIT - 6");

        //read line
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String readLine = reader.readLine();
        if ((readLine.trim().isEmpty() || !readLine.trim().matches("\\d"))) {
            throwError("Wrong input");
        } else if (Integer.parseInt(readLine) > 6 || Integer.parseInt(readLine) < 0) {
            throwError("Wrong input");
        } else {
            int option = Integer.parseInt(readLine);
            onMenuOptionPicked(option);
        }
    }

    private void onMenuOptionPicked(int option) throws IOException {
        switch (option) {
            case WarehouseConstants
                    .MENU_ITEM_DISPLAY_LIST:
                for (Order order : mainPresenter.getOrders()) {
                    printOrder(order);

                }
                printMenu(false);
                break;

            case WarehouseConstants
                    .MENU_ITEM_ADD_NEW_ORDER:

                int stockProductsCount;
                Order order = new Order();
                List<Product> selectedProducts = new ArrayList<>();

                if (customer == null) {
                    boolean success = createNewCustomer();
                    if (!success) {
                        printMenu(true);
                        break;
                    }

                } else {
                    System.out.println("Create order with new customer?(Y/N)");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                    String yesNo = reader.readLine();
                    if (yesNo.trim().equals("Y") || yesNo.trim().equals("y")) {
                        boolean success = createNewCustomer();
                        if (!success) {
                            printMenu(true);
                            break;
                        }
                    } else if (!yesNo.trim().equals("n") && !yesNo.trim().equals("N")) {
                        System.out.println("Wrong input");
                        printMenu(true);
                        break;
                    }
                }
                //assign customer to order
                order.setCustomer(customer);

                //fill order
                System.out.println("\n\n\n---NEW ORDER---\n");

                boolean selectAnother = true;
                while (selectAnother) {
                    stockProductsCount = mainPresenter.getStockProducts().size();
                    //show stock
                    System.out.println("Products currently in stock: ");
                    for (Product product : mainPresenter.getStockProducts()) {
                        System.out.println(String.format("%s qty: %d - %d", product.getTitle(), product.getQuantity(), product.getId()));
                    }


                    System.out.print("\nSelect product:");
                    BufferedReader productReader = new BufferedReader(new InputStreamReader(System.in));
                    String productID = productReader.readLine();
                    if (productID.trim().equals("exit")) {
                        printMenu(true);
                        break;
                    }
                    if ((productID.trim().isEmpty() || !productID.trim().matches("\\d"))) {
                        throwError("Wrong input");
                        break;
                    } else if (Integer.parseInt(productID) > stockProductsCount) {
                        throwError("Wrong input");
                        break;
                    }
                    Product selectedProduct = mainPresenter.getProductById(Integer.parseInt(productID));
                    if (selectedProduct.getQuantity() >= 1) {
                        addProductToOrder(selectedProducts, selectedProduct);
                        mainPresenter.updateProductQuantity(selectedProduct.getId(), selectedProduct.getQuantity() - 1);
                    } else {
                        System.out.println("Product is out of stock\n");
                    }

                    //another product loop
                    System.out.print("\nSelect another product - 1 \nBack to menu - 2\n");
                    BufferedReader anotherProductReader = new BufferedReader(new InputStreamReader(System.in));
                    String inputValue = anotherProductReader.readLine();

                    if ((inputValue.trim().isEmpty() || !inputValue.trim().matches("\\d"))) {
                        throwError("Wrong input");
                        break;
                    } else if (Integer.parseInt(inputValue) > 2 || Integer.parseInt(inputValue) < 0) {
                        throwError("Wrong input");
                        break;
                    }

                    if (Integer.parseInt(inputValue) == 2) {
                        selectAnother = false;
                        order.setProducts(selectedProducts);
                    }
                    System.out.println("--------------------------------------");
                }

                if (order.getProducts().size() >= 1) {
                    order.setStatus(WarehouseConstants.OrderStatus.RECEIVED);
                    order.setOrderNumber(mainPresenter.getUniqueOrderNumber());

                    mainPresenter.addOrder(order);
                    System.out.println("ORDER ADDED SUCCESSFULLY");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("--------------------------------------");

                }
                printMenu(true);
                break;

            case WarehouseConstants
                    .MENU_ITEM_FIND_ORDER:
                System.out.println("Find order by order number - 1");
                System.out.println("Find order by customer name/surname - 2");
                System.out.println("Find order by customer address - 3");
                System.out.println("Enter find method number: ");
                BufferedReader findMethodReader = new BufferedReader(new InputStreamReader(System.in));
                String findMethod = findMethodReader.readLine();
                if (findMethod.trim().isEmpty() || !findMethod.trim().matches("\\d")) {
                    throwError("Wrong input");
                    break;
                } else {
                    int method = Integer.parseInt(findMethod);
                    switch (method) {
                        case 1: {
                            //find by order number
                            while (true) {
                                System.out.println("Exit to menu - 0 ");
                                System.out.println("Enter order number (only numbers): ");

                                BufferedReader anotherProductReader = new BufferedReader(new InputStreamReader(System.in));
                                String orderNumber = anotherProductReader.readLine();
                                if (!orderNumber.trim().isEmpty() && findMethod.trim().matches("\\d")) {
                                    int orderNumber1 = Integer.parseInt(orderNumber);
                                    if (orderNumber1 == 0) {
                                        printMenu(true);
                                        return;
                                    }
                                    Order foundOrder = mainPresenter.getOrderByOrderNumber(orderNumber1);
                                    if (foundOrder.getStatus() == null) System.out.println("No order found");
                                    else {
                                        printOrder(foundOrder);

                                        boolean goToMenu = initOrderStateChange(Collections.singletonList(foundOrder));
                                        if (goToMenu) {
                                            printMenu(true);
                                            break;
                                        }
                                    }
                                } else {
                                    throwError("Wrong input");
                                    break;
                                }
                            }
                            printMenu(true);
                            return;
                        }

                        case 2:
                            //find by customer name/surname
                            while (true) {
                                System.out.println("Exit to menu - 'exit' ");
                                System.out.println("Enter customer name/surname: ");
                                BufferedReader nameSurnameReader = new BufferedReader(new InputStreamReader(System.in));
                                String nameSurname = nameSurnameReader.readLine();
                                if (!nameSurname.trim().isEmpty()) {
                                    if (nameSurname.equalsIgnoreCase("exit")) {
                                        printMenu(true);
                                        return;
                                    }
                                    List<Order> foundOrders = mainPresenter.getOrdersByNameOrSurname(nameSurname);
                                    if (foundOrders.size() == 0) System.out.println("No order found");
                                    else {
                                        for (Order o : foundOrders)
                                            printOrder(o);

                                        boolean goToMenu = initOrderStateChange(foundOrders);
                                        if (goToMenu) {
                                            printMenu(true);
                                            return;
                                        }
                                    }
                                } else {
                                    throwError("Wrong input");
                                    break;
                                }
                            }
                            printMenu(true);
                            return;

                        case 3:
                            while (true) {
                                System.out.println("Exit to menu - 'exit' ");
                                System.out.println("Enter customer address: ");
                                BufferedReader addressReader = new BufferedReader(new InputStreamReader(System.in));
                                String address = addressReader.readLine();
                                if (!address.trim().isEmpty()) {
                                    if (address.equalsIgnoreCase("exit")) {
                                        printMenu(true);
                                        return;
                                    }
                                    List<Order> foundOrders = mainPresenter.getOrdersByAddress(address);
                                    if (foundOrders.size() == 0)
                                        System.out.println("No order found");
                                    else {
                                        for (Order o : foundOrders)
                                            printOrder(o);

                                        boolean goToMenu = initOrderStateChange(foundOrders);
                                        if (goToMenu) {
                                            printMenu(true);
                                            return;
                                        }
                                    }
                                } else {
                                    throwError("Wrong input");
                                    break;
                                }
                                printMenu(true);
                                return;
                            }
                    }
                }
                printMenu(true);
                return;

            case WarehouseConstants
                    .MENU_ITEM_DROP_ORDERS:
                while (true) {
                    System.out.println("\nAll orders will be deleted! Are you sure? (Y/N)\n Y- Start fresh copy \n N- Go back to menu");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                    String yesNo = reader.readLine();
                    if (yesNo.trim().equals("Y") || yesNo.trim().equals("y")) {
                        mainPresenter.dropAllTables();
                        customer = null;
                        printMenu(true);
                        break;
                    } else if (yesNo.trim().equals("n") || yesNo.trim().equals("N")) {
                        printMenu(true);
                        break;
                    } else {
                        System.out.println("Wrong input");
                    }
                }

            case WarehouseConstants.MENU_ITEM_DISPLAY_FINISHED_ORDERS:
                List<Order> finishedOrders = mainPresenter.getFinishedOrders();
                if (finishedOrders.size() < 1) {
                    System.out.println("No orders found");
                }
                for (Order finishedOrder : finishedOrders)
                    printOrder(finishedOrder);

                printMenu(false);
                break;
            case WarehouseConstants
                    .MENU_ITEM_EXIT:
                System.out.println("EXITING...");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.exit(0);
                break;
        }

    }

    private boolean createNewCustomer() throws IOException {
        customer = null;
        customer = new Customer();

        System.out.println("\n\n\n---Fill your details---\n");
        //fill name
        System.out.print("Name: ");
        BufferedReader nameReader = new BufferedReader(new InputStreamReader(System.in));
        String name = nameReader.readLine();

        if (name.trim().isEmpty()) {
            customer = null;
            throwError("Input can't be empty");
            return false;
        }
        customer.setName(name);

        //fill surname
        System.out.print("Surname: ");
        BufferedReader surnameReader = new BufferedReader(new InputStreamReader(System.in));
        String surname = surnameReader.readLine();
        if (surname.trim().isEmpty()) {
            customer = null;
            throwError("Input can't be empty");
            return false;
        }
        customer.setSurname(surname);

        //fill address
        System.out.print("Address: ");
        BufferedReader addressReader = new BufferedReader(new InputStreamReader(System.in));
        String address = addressReader.readLine();
        if (address.trim().isEmpty()) {
            customer = null;
            throwError("Input can't be empty");
            return false;
        }
        customer.setAddress(address);

        System.out.println("--------------------------------------");
        return true;
    }

    private boolean initOrderStateChange(List<Order> orders) throws IOException {
        boolean changeState = true;
        while (changeState) {
            System.out.println("--- Ship order ---");
            System.out.println("Enter 'exit' to go back to menu or order id (number): ");
            BufferedReader orderIdReader = new BufferedReader(new InputStreamReader(System.in));
            String operation = orderIdReader.readLine();
            if (operation.trim().equalsIgnoreCase("exit")) {
                return true;
            }
            if (operation.trim().isEmpty() || !operation.trim().matches("\\d")) {
                System.out.println("Wrong input");
            } else {
                int orderId = Integer.parseInt(operation);

                if (orderId > orders.size() || orderId <= 0) {
                    System.out.println("Order not found");
                    continue;
                }

                Order foundOrder = orders.stream().filter(order -> order.getId() == orderId).findFirst().get();
                if (foundOrder != null) {
                    mainPresenter.changeOrderStatus(foundOrder, WarehouseConstants.OrderStatus.DONE);
                    System.out.println("Order status changed successfully");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return true;
                } else System.out.println("Order not found");

            }
        }
        return true;
    }

    private void printOrder(Order order) {
        if (order == null) {
            System.out.println("Order not found!");
            return;
        }
        System.out.println("--------------------------------------");

        System.out.println("ORDER_ID: " + order.getId());
        System.out.println("ORDER_NUMBER: " + order.getOrderNumber());
        System.out.println("ORDER_STATUS: " + order.getStatus());
        System.out.println("ORDERED_PRODUCTS: ");
        for (Product product : order.getProducts()) {
            System.out.println("    PRODUCT_ID: " + product.getId());
            System.out.println("    PRODUCT_TITLE: " + product.getTitle());
            System.out.println("    PRODUCT_QUANTITY: " + product.getQuantity());
            System.out.println("------------------------");
        }
        System.out.println("CUSTOMER INFO: ");
        System.out.println("    CUSTOMER_NAME: " + order.getCustomer().getName());
        System.out.println("    CUSTOMER_SURNAME: " + order.getCustomer().getSurname());
        System.out.println("    CUSTOMER_ADDRESS: " + order.getCustomer().getAddress());
        System.out.println("    CUSTOMER_ID: " + order.getCustomer().getId());

        System.out.println("--------------------------------------");
    }

    private List<Product> addProductToOrder(List<Product> products, Product selectedProduct) {
        //if selected product is already in the basket, just increase the quantity
        for (Product p : products) {
            if (p.getId() == selectedProduct.getId()) {
                p.setQuantity(p.getQuantity() + 1);
                return products;
            }
        }

        //else add product to the basket
        Product newProduct = new Product();
        newProduct.setId(selectedProduct.getId());
        newProduct.setTitle(selectedProduct.getTitle());
        newProduct.setQuantity(1);
        products.add(newProduct);
        return products;

    }

    private void initPresenter() {
        mainPresenter = new MainPresenter();
    }

    public void throwError(String message) {
        try {
            System.out.println(message);
            printMenu(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
