package lt.vaskevicius.model;

import java.util.List;

public class Order {

    private int id;
    private int orderNumber;
    private Customer customer;
    private String status;
    private List<Product> products;

    public Order() {
    }

    public Order(int orderNumber, Customer customer, String status, List<Product> products) {
        this.orderNumber = orderNumber;
        this.customer = customer;
        this.status = status;
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
