package lt.vaskevicius.utils;

public abstract class WarehouseConstants {

    //Menu items
    public static final int MENU_ITEM_DISPLAY_LIST = 1;
    public static final int MENU_ITEM_ADD_NEW_ORDER = 2;
    public static final int MENU_ITEM_FIND_ORDER = 3;
    public static final int MENU_ITEM_DROP_ORDERS = 4;
    public static final int MENU_ITEM_EXIT = 6;
    public static final int MENU_ITEM_DISPLAY_FINISHED_ORDERS = 5;


    //Order status
    public static class OrderStatus {
        public static final String RECEIVED = "received";
        public static final String PROCESSING = "processing";
        public static final String DONE = "shipped";
    }


}
