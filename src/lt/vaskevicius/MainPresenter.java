package lt.vaskevicius;

import lt.vaskevicius.db.JdbcSqliteConnection;
import lt.vaskevicius.model.Order;
import lt.vaskevicius.model.Product;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class MainPresenter extends MainView {

    private JdbcSqliteConnection connection;

    public MainPresenter() {
        connection = new JdbcSqliteConnection(true);
    }

    public void updateProductQuantity(int productId, int quantity) {
        try {
            connection.updateProductQuantity(productId, quantity);
        } catch (SQLException throwables) {
            throwError(throwables.getMessage());
        }
    }

    public void addOrder(Order order) {
        try {
            connection.addOrder(order);
        } catch (SQLException throwables) {
            throwError(throwables.getMessage());
        }
    }

    public void changeOrderStatus(Order order, String orderStatus) {
        try {
            connection.changeOrderStatus(order, orderStatus);
        } catch (SQLException throwables) {
            throwError(throwables.getMessage());
        }
    }

    public void dropAllTables() {
        connection = new JdbcSqliteConnection(true);
    }

    public List<Order> getFinishedOrders() {
        try {
            return connection.getFinishedOrders();
        } catch (SQLException throwables) {
            throwError(throwables.getMessage());
        }
        return null;
    }

    public List<Order> getOrdersByNameOrSurname(String nameOrSurname) {
        try {
            return connection.getOrders().stream().filter(order -> order.getCustomer().getName().equalsIgnoreCase(nameOrSurname) || order.getCustomer().getSurname().equalsIgnoreCase(nameOrSurname)).collect(Collectors.toList());
        } catch (SQLException throwables) {
            throwError(throwables.getMessage());
        }
        return null;
    }

    public List<Order> getOrdersByAddress(String address) {
        try {
            return connection.getOrders().stream().filter(order -> order.getCustomer().getAddress().contains(address)).collect(Collectors.toList());
        } catch (SQLException throwables) {
            throwError(throwables.getMessage());
        }
        return null;
    }

    public Order getOrderByOrderNumber(int orderNumber) {
        try {
            return connection.getOrderByOrderNumber(orderNumber);
        } catch (SQLException throwables) {
            throwError(throwables.getMessage());
        }
        return null;
    }

    public int getUniqueOrderNumber() {
        AtomicLong idCounter = new AtomicLong(100);
        long timestamp = System.currentTimeMillis();
        long nextLong = idCounter.incrementAndGet();
        return (int) (timestamp + nextLong);
    }

    public List<Product> getStockProducts() {
        try {
            return connection.getStockProducts();
        } catch (SQLException throwables) {
            throwError(throwables.getMessage());
        }
        return new ArrayList<>();
    }

    public Product getProductById(int id) {
        try {
            return connection.getProductByProductId(id);
        } catch (SQLException throwables) {
            throwError(throwables.getMessage());
        }
        return new Product();
    }

    public List<Order> getOrders() {
        try {
            return connection.getOrders();
        } catch (SQLException throwables) {
            throwError(throwables.getMessage());
        }
        return new LinkedList<>();
    }


}
