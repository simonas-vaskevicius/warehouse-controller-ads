package lt.vaskevicius.db;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lt.vaskevicius.model.Customer;
import lt.vaskevicius.model.Order;
import lt.vaskevicius.model.Product;
import lt.vaskevicius.utils.WarehouseConstants;

import java.lang.reflect.Type;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JdbcSqliteConnection {

    private Statement stmt;
    private Connection conn;
    private Gson gson;

    public JdbcSqliteConnection(boolean dropExistingTables) {
        try {
            Class.forName("org.sqlite.JDBC");
            String dbURL = "jdbc:sqlite:warehouse.db";
            conn = DriverManager.getConnection(dbURL);
            stmt = conn.createStatement();
            createSchemas(stmt, dropExistingTables);
            mockProducts();
            gson = new Gson();

        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (
                SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void openConnection() {
        try {
            if (conn.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                String dbURL = "jdbc:sqlite:warehouse.db";
                conn = DriverManager.getConnection(dbURL);
                stmt = conn.createStatement();
            }

        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (
                SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void closeConnection() {
        try {
            stmt.close();
            conn.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void createSchemas(Statement statement, boolean dropExistingTables) throws SQLException {
        if (dropExistingTables) {
            //drop existing tables
            String dropOrdersTable = "DROP TABLE IF EXISTS ORDERS";
            String dropFinishedOrdersTable = "DROP TABLE IF EXISTS FINISHED_ORDERS";
            String dropCustomersTable = "DROP TABLE IF EXISTS CUSTOMERS";
            String dropProductsTable = "DROP TABLE IF EXISTS PRODUCTS";

            statement.executeUpdate(dropOrdersTable);
            statement.executeUpdate(dropFinishedOrdersTable);
            statement.executeUpdate(dropProductsTable);
            statement.executeUpdate(dropCustomersTable);
        }

        //create new tables
        String createOrdersTable = "CREATE TABLE ORDERS " +
                "(ID INTEGER  PRIMARY KEY     AUTOINCREMENT," +
                " PRODUCTS               TEXT    NOT NULL, " +
                " CUSTOMER_ID            INT    NOT NULL, " +
                " ORDER_NUMBER           INT    NOT NULL, " +
                " ORDER_STATUS           TEXT    NOT NULL)";

        String createFinishedOrderTable = "CREATE TABLE FINISHED_ORDERS " +
                "(ID INTEGER  PRIMARY KEY     AUTOINCREMENT," +
                " PRODUCTS               TEXT    NOT NULL, " +
                " CUSTOMER_ID            INT    NOT NULL, " +
                " ORDER_NUMBER           INT    NOT NULL, " +
                " ORDER_STATUS           TEXT    NOT NULL)";

        String createCustomersTable = "CREATE TABLE CUSTOMERS " +
                "(ID INTEGER  PRIMARY KEY     AUTOINCREMENT," +
                " CUSTOMER_NAME          TEXT    NOT NULL, " +
                " CUSTOMER_SURNAME       TEXT    NOT NULL, " +
                " CUSTOMER_ADDRESS       TEXT    NOT NULL)";

        String createProductsTable = "CREATE TABLE PRODUCTS " +
                "(ID INTEGER  PRIMARY KEY     AUTOINCREMENT," +
                " PRODUCT_TITLE          TEXT    NOT NULL, " +
                " PRODUCT_QUANTITY       INT    NOT NULL)";


        //execute sql script
        statement.executeUpdate(createOrdersTable);
        statement.executeUpdate(createFinishedOrderTable);
        statement.executeUpdate(createProductsTable);
        statement.executeUpdate(createCustomersTable);
        closeConnection();
    }

    public void addOrder(Order order) throws SQLException {
        openConnection();

        //create products json
        String products = gson.toJson(order.getProducts());

        String insertOrder = "INSERT INTO ORDERS (ID,PRODUCTS,CUSTOMER_ID,ORDER_NUMBER,ORDER_STATUS) " +
                String.format("VALUES (NULL,'%s',%d,%d,'%s');", products, order.getCustomer().getId(), order.getOrderNumber() + 1000, order.getStatus());

        String insertCustomer = "INSERT INTO CUSTOMERS (ID, CUSTOMER_NAME,CUSTOMER_SURNAME,CUSTOMER_ADDRESS) " +
                String.format("VALUES (NULL, '%s', '%s', '%s');", order.getCustomer().getName(), order.getCustomer().getSurname(), order.getCustomer().getAddress());

        stmt.executeUpdate(insertOrder);
        stmt.executeUpdate(insertCustomer);
        closeConnection();
    }

    public void addFinishedOrder(Order order) throws SQLException {
        openConnection();

        //create products json
        String products = gson.toJson(order.getProducts());

        String insertOrder = "INSERT INTO FINISHED_ORDERS (ID,PRODUCTS,CUSTOMER_ID,ORDER_NUMBER,ORDER_STATUS) " +
                String.format("VALUES (NULL,'%s',%d,%d,'%s');", products, order.getCustomer().getId(), order.getOrderNumber() + 1000, order.getStatus());

        stmt.executeUpdate(insertOrder);
        closeConnection();
    }

    public void updateProductQuantity(int productId, int quantity) throws SQLException {
        openConnection();
        String query = "UPDATE PRODUCTS SET PRODUCT_QUANTITY=? WHERE ID=?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setInt(1, quantity);
        statement.setInt(2, productId);
        statement.executeUpdate();
        closeConnection();
    }

    public void addProduct(Product product) throws SQLException {
        openConnection();
        String insertProduct = "INSERT INTO PRODUCTS (ID,PRODUCT_TITLE,PRODUCT_QUANTITY)" +
                String.format("VALUES (NULL, '%s', %d)", product.getTitle(), product.getQuantity());

        stmt.executeUpdate(insertProduct);
        closeConnection();
    }

    public List<Product> getStockProducts() throws SQLException {
        openConnection();
        ResultSet productRS = stmt.executeQuery("SELECT * FROM PRODUCTS;");
        List<Product> products = new ArrayList<>();
        while (productRS.next()) {
            Product product = new Product();
            product.setId(productRS.getInt("ID"));
            product.setTitle(productRS.getString("PRODUCT_TITLE"));
            product.setQuantity(productRS.getInt("PRODUCT_QUANTITY"));
            products.add(product);
        }
        return products;
    }

    public List<Order> getOrders() throws SQLException {
        openConnection();
        LinkedList<Order> orders = new LinkedList<>();
        ResultSet orderRS = stmt.executeQuery("SELECT * FROM ORDERS;");
        Gson gson = new Gson();
        while (orderRS.next()) {
            Order order = new Order();
            Customer customer = new Customer();
            order.setId(orderRS.getInt("ID"));

            //convert json products list to object
            Type type = new TypeToken<List<Product>>() {
            }.getType();
            List<Product> prodList = gson.fromJson(orderRS.getString("PRODUCTS"), type);
            order.setProducts(prodList);

            order.setOrderNumber(orderRS.getInt("ORDER_NUMBER"));
            order.setStatus(orderRS.getString("ORDER_STATUS"));
            order.setCustomer(customer);
            orders.add(order);
        }

        closeConnection();

        //fill order model with customers
        //order id is associated with customer id
        for (Order order : orders) {
            order.setCustomer(getCustomerByOrderId(order.getId()));
        }

        return orders;
    }

    public List<Order> getFinishedOrders() throws SQLException {
        openConnection();
        List<Order> orders = new ArrayList<>();
        ResultSet orderRS = stmt.executeQuery("SELECT * FROM FINISHED_ORDERS;");
        Gson gson = new Gson();
        while (orderRS.next()) {
            Order order = new Order();
            Customer customer = new Customer();
            customer.setId(orderRS.getInt("CUSTOMER_ID"));
            order.setId(orderRS.getInt("ID"));
            order.setCustomer(customer);

            //convert json products list to object
            Type type = new TypeToken<List<Product>>() {
            }.getType();
            List<Product> prodList = gson.fromJson(orderRS.getString("PRODUCTS"), type);
            order.setProducts(prodList);

            order.setOrderNumber(orderRS.getInt("ORDER_NUMBER"));
            order.setStatus(orderRS.getString("ORDER_STATUS"));
            orders.add(order);
        }

        closeConnection();

        //fill order model with customers
        //order id is associated with customer id
        for (Order order : orders) {
            Customer customer = getCustomerByCustomerId(order.getCustomer().getId());
            order.setCustomer(customer);
        }

        return orders;
    }

    public Order getOrderByOrderNumber(int orderNumber) throws SQLException {
        openConnection();
        Order order = new Order();
        ResultSet orderRS = stmt.executeQuery(String.format("SELECT * FROM ORDERS WHERE ORDER_NUMBER = %d;", orderNumber));
        while (orderRS.next()) {
            Customer customer = new Customer();
            order.setId(orderRS.getInt("ID"));

            //convert json products list to object
            Type type = new TypeToken<List<Product>>() {
            }.getType();
            List<Product> prodList = gson.fromJson(orderRS.getString("PRODUCTS"), type);
            order.setProducts(prodList);

            order.setOrderNumber(orderRS.getInt("ORDER_NUMBER"));
            order.setStatus(orderRS.getString("ORDER_STATUS"));
            order.setCustomer(customer);
        }
        closeConnection();
        return order;
    }

    public void deleteOrderFromOrdersTable(int orderId) throws SQLException {
        openConnection();
        String sql = "DELETE FROM ORDERS WHERE id = ?";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setInt(1, orderId);
        pstmt.executeUpdate();
        closeConnection();
    }

    public void changeOrderStatus(Order order, String orderStatus) throws SQLException {
        if (orderStatus.equals(WarehouseConstants.OrderStatus.DONE)) {
            order.setStatus(orderStatus);
            addFinishedOrder(order);
            deleteOrderFromOrdersTable(order.getId());
        } else {
            openConnection();
            String query = "UPDATE ORDERS SET ORDER_STATUS=? WHERE ID=?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setInt(2, order.getId());
            statement.setString(1, orderStatus);
            statement.executeUpdate();
            closeConnection();
        }
    }

    public Customer getCustomerByOrderId(int orderId) throws SQLException {
        openConnection();
        Customer customer = new Customer();
        ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM CUSTOMERS WHERE ID = %d;", orderId));
        while (rs.next()) {
            customer.setId(rs.getInt("ID"));
            customer.setName(rs.getString("CUSTOMER_NAME"));
            customer.setSurname(rs.getString("CUSTOMER_SURNAME"));
            customer.setAddress(rs.getString("CUSTOMER_ADDRESS"));
        }
        closeConnection();
        return customer;
    }

    public Customer getCustomerByCustomerId(int customerId) throws SQLException {
        openConnection();
        Customer customer = new Customer();
        ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM CUSTOMERS WHERE ID = %d;", customerId));
        while (rs.next()) {
            customer.setId(rs.getInt("ID"));
            customer.setName(rs.getString("CUSTOMER_NAME"));
            customer.setSurname(rs.getString("CUSTOMER_SURNAME"));
            customer.setAddress(rs.getString("CUSTOMER_ADDRESS"));
        }
        closeConnection();
        return customer;
    }

    public Product getProductByProductId(int id) throws SQLException {
        openConnection();
        Product product = new Product();
        ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM PRODUCTS WHERE ID = %d;", id));
        while (rs.next()) {
            product.setId(rs.getInt("ID"));
            product.setQuantity(rs.getInt("PRODUCT_QUANTITY"));
            product.setTitle(rs.getString("PRODUCT_TITLE"));
        }
        closeConnection();

        return product;
    }

    private void mockProducts() {
        try {
            addProduct(new Product("Cheese", 4));
            addProduct(new Product("Crisps", 6));
            addProduct(new Product("Pizza", 1));
            addProduct(new Product("Chocolate", 2));
            addProduct(new Product("Self-raising flour", 59));
            addProduct(new Product("Ground almonds", 0));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
